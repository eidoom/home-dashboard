# home-dashboard

Used <https://www.freecodecamp.org/news/learn-bootstrap-4-in-30-minute-by-building-a-landing-page-website-guide-for-beginners-f64e03833f33/> for reference.

## Installation
```shell
git clone git@gitlab.com:eidoom/home-dashboard.git
cd home-dashboard
npm install
```

## Run
For testing:
```shell
cd home-dashboard
python -m http.server 8000
```
and view at <http://localhost:8000/>.

## TODO
### Minor
* Click Factorio card to copy server address, as in [this tutorial](https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_copy_clipboard2)
* Fix navbar dropdown menu 
* Add visual feedback when hovering over cards
### Major
* Overhaul appearance